﻿import React, { Component } from "react";

import { Tooltip, OverlayTrigger } from "react-bootstrap";

import Checkbox from "components/CustomCheckbox/CustomCheckbox.jsx";

import Button from "components/CustomButton/CustomButton.jsx";
import firebase from '../../firebase';



export class AddVolunteer extends Component {


  constructor() {
    super();
    this.ref = firebase.firestore().collection('volunteers');
    this.state = {
      name: '',
      mobile: '',
      age: ''
    };
  }
  onChange = (e) => {
    const state = this.state
    state[e.target.name] = e.target.value;
    this.setState(state);
  }

  onSubmit = (e) => {
    e.preventDefault();

    const { name, mobile, age } = this.state;

    this.ref.add({
      name,
      mobile,
      age
    }).then((docRef) => {
      this.setState({
        name: '',
        mobile: '',
        age: ''
      });
      this.props.history.push("/")
    })
    .catch((error) => {
      console.error("Error adding document: ", error);
    });
  }

  render() {
    const { name, mobile, age } = this.state;
    return (
      <div class="container">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">
              ADD BOARD
            </h3>
          </div>
          <div class="panel-body">
            
            <form onSubmit={this.onSubmit}>
              <div class="form-group">
                <label for="name">Name:</label>
                <input type="text" class="form-control" name="name" value={name} onChange={this.onChange} placeholder="Name" />
              </div>
              <div class="form-group">
                <label for="mobile">Mobile:</label>
                <input type="text" class="form-control" name="mobile" value={mobile} onChange={this.onChange} placeholder="Mobile" />
              </div>
              <div class="form-group">
                <label for="age">Age:</label>
                <input type="text" class="form-control" name="age" value={age} onChange={this.onChange} placeholder="Age" />
              </div>
              <button type="submit" class="btn btn-success">Submit</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default AddVolunteer;