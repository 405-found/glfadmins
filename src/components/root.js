import React, {Component} from 'react'
import {init as firebaseInit} from '../firebase'
import {browserHistory} from 'react-router'//this may not need
import Routes from './routes'
export default class Root extends Component {
  constructor(props) {
    super(props)
    firebaseInit()
  }
render() {
    return (
      <Routes history={browserHistory}/>
    )
  }
}