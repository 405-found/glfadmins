import React, { Component } from "react";

import { Row, Col, Grid, Table } from "react-bootstrap";
import Card from "components/Card/Card";
import UserCard from "components/UserCard/UserCard";
import { thArray, tdArray } from "variables/Variables.jsx";
import Button from "components/CustomButton/CustomButton";

class Redeem extends Component {
  state = {};
  render() {
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            {/* <Col lg={4}></Col> */}
            <Col md={4}>
              <UserCard
                name="Ilthizam"
                avatar="https://scontent.fcmb4-1.fna.fbcdn.net/v/t1.0-9/61967929_2241620242539863_1903708511234162688_n.jpg?_nc_cat=107&_nc_oc=AQkhyfY1VnxWinu7DLE6cqDUVNJ5-LleiV8YfdONTntCHnMBBmzP0jQBGumi4wheBBiQGCY2p6yPyyuWYjhOB-fl&_nc_ht=scontent.fcmb4-1.fna&oh=9996c7532b67352ce207d9af923d68e9&oe=5E156E6D"
                userName="ilthizzam@gmail.com"
                bgImage="https://ununsplash.imgix.net/photo-1431578500526-4d9613015464?fit=crop&fm=jpg&h=300&q=75&w=400"
                description={
                  <span>
                    Sri Lanka
                    <br />
                    Galle Fort
                    <br />
                    +94765465181
                  </span>
                }
              />
            </Col>
            <Col lg={8}>
              <Card
                title="Redeemed Tickets"
                ctTableFullWidth
                ctTableResponsive
                content={
                  <Table striped hover>
                    <thead>
                      <tr>
                        <th>Id</th>
                        <th>Name</th>
                        <th>Amount</th>
                        <th>No of tickets</th>
                      </tr>
                    </thead>
                    <tbody>
                      {tdArray.map((prop, key) => {
                        return (
                          <tr key={key}>
                            {prop.map((prop, key) => {
                              return <td key={key}>{prop}</td>;
                            })}
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                }
              />
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <Card
                title="Redeem Ticket"
                category="Here is a subtitle for this table"
                ctTableFullWidth
                ctTableResponsive
                content={
                  <Table striped hover>
                    <thead>
                      <tr>
                        {/* {thArray.map((prop, key) => {
                          return <th key={key}>{prop}</th>;
                        })} */}
                        <th>Id</th>
                        <th>Event Name</th>
                        <th>Amount</th>
                        <th>No of tickets</th>
                      </tr>
                    </thead>
                    <tbody>
                      {tdArray.map((prop, key) => {
                        return (
                          <tr key={key}>
                            {prop.map((prop, key) => {
                              return <td key={key}>{prop}</td>;
                            })}
                            <td>
                              <Button bsStyle="danger" fill round>
                                Redeem ticket
                              </Button>
                            </td>
                          </tr>
                        );
                      })}
                    </tbody>
                  </Table>
                }
              />
            </Col>
            <Row>
              <Col lg={5}></Col>
              <Col>
                <Card
                  content={
                    <Button bsStyle="success" round fill>
                      Redeem All tickets
                    </Button>
                  }
                />
              </Col>
            </Row>
          </Row>
        </Grid>
      </div>
    );
  }
}

export default Redeem;
