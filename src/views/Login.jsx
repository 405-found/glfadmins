import React, { Component } from "react";
import Card from "components/Card/Card";
import { Grid, Row, Col, Table } from "react-bootstrap";
import fire from "config/Auth";


class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
        email:'',
        password:''
    };
  }

  login=(e)=>{
    e.preventDefault();
    fire.auth().signInWithEmailAndPassword(this.state.email,this.state.password).then((u)=>{    
    }).catch((error)=>{
        console.log(error);
    })
  }

  handleChange=(e)=>{
    this.setState({[e.target.name]:e.target.value});
  }

  render() {
    return (
      <div className="content">
        <Row>
            <Col lg={4}></Col>
          <Col lg={4}>
            <Card
              title="Galle Literary Festival 2019"
              content={
                <form>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Email address</label>
                    <input
                      type="email"
                      class="form-control"
                      id="exampleInputEmail1"
                      aria-describedby="emailHelp"
                      placeholder="Enter email"
                      name="email"
                      value={this.state.email}
                      onChange={this.handleChange}
                    ></input>
                    <small id="emailHelp" class="form-text text-muted">
                      We'll never share your email with anyone else.
                    </small>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Password</label>
                    <input
                      type="password"
                      class="form-control"
                      id="exampleInputPassword1"
                      placeholder="Password"
                      name="password"
                      value={this.state.password}
                      onChange={this.handleChange}
                    ></input>
                  </div>
                  <div class="form-check">
                    <label class="form-check-label">
                      <input
                        class="form-check-input"
                        type="checkbox"
                        value=""
                      ></input>
                      Option one is this
                      <span class="form-check-sign">
                        <span class="check"></span>
                      </span>
                    </label>
                  </div>

                  <button onClick={this.login} type="submit" class="btn btn-primary">
                    Login
                  </button>
                </form>
              }
            />
          </Col>
        </Row>
      </div>
    );
  }
}

export default Login;
