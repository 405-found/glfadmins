
import firebase from 'firebase';

const firebaseConfig = {
    apiKey: "AIzaSyBWrugGps87Gjqv3xR0tNU5WP4W_zZAmLk",
    authDomain: "galleliteraryfestival-f14e2.firebaseapp.com",
    databaseURL: "https://galleliteraryfestival-f14e2.firebaseio.com",
    projectId: "galleliteraryfestival-f14e2",
    storageBucket: "galleliteraryfestival-f14e2.appspot.com",
    messagingSenderId: "551694234288",
    appId: "1:551694234288:web:ff65fc865c109f6840c175"
};
const fire=firebase.initializeApp(firebaseConfig);
export default fire;