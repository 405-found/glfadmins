import React, { Component } from 'react';
import fire from 'config/Auth';
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";

import AdminLayout from "layouts/Admin.jsx";
import Login from 'views/Login';


class App extends Component {
    constructor(props) {
        super(props);
        this.state = { 
            user:{},    
         }
    }

    authListner(){
        fire.auth().onAuthStateChanged((user)=>{
            console.log(user);
            if(user){
                this.setState({user});
            }else{
                this.setState({user:null});
            }
        });
    }

    componentDidMount(){
        this.authListner();
    }

    render() { 
        return ( <div>
            {this.state.user ? (<BrowserRouter>
                <Switch>
                    <Route path="/admin" render={props => <AdminLayout {...props} />} />
                    <Redirect from="/" to="/admin/dashboard" />
                </Switch>
            </BrowserRouter>):(<Login/>)}
        </div> );
    }
}
 
export default App;