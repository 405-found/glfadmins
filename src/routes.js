import Dashboard from "views/Dashboard.jsx";
import UserProfile from "views/UserProfile.jsx";
import TableList from "views/TableList.jsx";
import Typography from "views/Typography.jsx";
import Icons from "views/Icons.jsx";
import Maps from "views/Maps.jsx";
import Notifications from "views/Notifications.jsx";
import Upgrade from "views/Upgrade.jsx";
import BoxofficeDashboard from "views/BoxofficeDashboard";
import Redeem from "views/Redeem";

const dashboardRoutes = [
  // {
  //   path: "/dashboard",
  //   name: "Dashboard",
  //   icon: "pe-7s-graph",
  //   component: Dashboard,
  //   layout: "/admin"
  // },
  {
    path : "/das",
    name : "BoxOffice",
    icon: "pe-7s-cash",
    component : BoxofficeDashboard,
    layout : "/admin"

  },
  {
    path: "/redeem",
    name: "Redeem",
    icon: "pe-7s-ticket",
    component: Redeem,
    layout: "/admin"
  },
  {
    path: "/user",
    name: "Staff Management",
    icon: "pe-7s-user",
    component: UserProfile,
    layout: "/admin"
  },
  
  // {
  //   path: "/table",
  //   name: "Add Volunteers",
  //   icon: "pe-7s-note2",
  //   component: AddForm,
  //   layout: "/admin"
  // },
  {
    path: "/addvolunteer",
    name: "Search Volunteers",
    icon: "pe-7s-note2",
    component: TableList,
    layout: "/admin"
  },
  {
    path: "/typography",
    name: "Mail",
    icon: "pe-7s-mail",
    component: Typography,
    layout: "/admin"
  },
  {
    path: "/icons",
    name: "Assign Tasks",
    icon: "pe-7s-science",
    component: Icons,
    layout: "/admin"
  },
  {
    path: "/typography",
    name: "Group Chat",
    icon: "pe-7s-news-paper",
    component: Typography,
    layout: "/admin"
  },

  {
    path: "/maps",
    name: "Track Location",
    icon: "pe-7s-map-marker",
    component: Maps,
    layout: "/admin"
  },
  {
    path: "/notifications",
    name: "Notifications",
    icon: "pe-7s-bell",
    component: Notifications,
    layout: "/admin"
  }
  // {
  //   upgrade: true,
  //   path: "/upgrade",
  //   name: "Upgrade to PRO",
  //   icon: "pe-7s-rocket",
  //   component: Upgrade,
  //   layout: "/admin"
  // }
];

export default dashboardRoutes;
